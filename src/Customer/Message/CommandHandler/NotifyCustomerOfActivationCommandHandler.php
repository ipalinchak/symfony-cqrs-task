<?php

namespace App\Customer\Message\CommandHandler;

use App\Customer\Message\Command\NotifyCustomerOfActivationCommand;
use App\Customer\Repository\CustomerRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class NotifyCustomerOfActivationCommandHandler implements MessageHandlerInterface
{
    private CustomerRepositoryInterface $repository;
    private LoggerInterface $logger;

    public function __construct(CustomerRepositoryInterface $repository, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    public function __invoke(NotifyCustomerOfActivationCommand $command): void
    {
        $customer = $this->repository->findById($command->getId());

        if (!$customer) {
            throw new \RuntimeException('Customer with id '.$command->getId().' not found!');
        }

        // notify customer via mail
        $this->logger->info('Customer activated. Sending notification now.');
    }
}
