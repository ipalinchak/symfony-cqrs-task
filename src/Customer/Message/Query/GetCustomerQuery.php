<?php

namespace App\Customer\Message\Query;

use Symfony\Component\Validator\Constraints as Assert;

final class GetCustomerQuery// implements RequestDto
{
    /**
     * @Assert\NotNull(message="Id should not be null.")
     * @Assert\Positive(message="Id should be a positive integer.")
     */
    private ?int $id = null;

    public function __construct(?int $id)
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}
