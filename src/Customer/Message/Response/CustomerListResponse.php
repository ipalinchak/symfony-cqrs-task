<?php

namespace App\Customer\Message\Response;

use App\Entity\Customer;
use Nelmio\ApiDocBundle\Annotation\Model;

final class CustomerListResponse
{
    /**
     * @var array<CustomerResponse>
     */
    private array $customers;
    private int $total;

    /**
     * @param array<Customer> $customers
     */
    public function __construct(array $customers, int $total)
    {
        $this->customers = [];
        $this->total = $total;

        foreach ($customers as $customer) {
            $this->customers[] = new CustomerResponse($customer);
        }
    }

    /**
     *
     * @return array<CustomerResponse>
     */
    public function getCustomers(): array
    {
        return $this->customers;
    }

    public function getTotal(): int
    {
        return $this->total;
    }
}
