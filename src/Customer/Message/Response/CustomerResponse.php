<?php

namespace App\Customer\Message\Response;

use App\Entity\Customer;

final class CustomerResponse
{
    private Customer $entity;

    public function __construct(Customer $entity)
    {
        $this->entity = $entity;
    }

    public function getId(): int
    {
        return $this->entity->getId();
    }

    public function getName(): string
    {
        return $this->entity->getName();
    }

    public function isActive(): bool
    {
        return $this->entity->getIsActive();
    }
}
