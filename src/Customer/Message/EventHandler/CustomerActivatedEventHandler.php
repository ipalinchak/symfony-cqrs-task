<?php

namespace App\Customer\Message\EventHandler;

use App\Customer\Message\Command\NotifyCustomerOfActivationCommand;
use App\Customer\Message\Event\CustomerActivatedEvent;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class CustomerActivatedEventHandler implements MessageHandlerInterface
{
    private MessageBusInterface $commandBus;

    public function __construct(MessageBusInterface $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(CustomerActivatedEvent $event): void
    {
        $command = new NotifyCustomerOfActivationCommand($event->getId());
        $this->commandBus->dispatch(new Envelope($command));
    }
}
