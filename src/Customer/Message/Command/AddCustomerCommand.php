<?php

namespace App\Customer\Message\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class AddCustomerCommand
{
    /**
     * @Assert\NotNull(message="Id should not be null.")
     * @Assert\Positive(message="Id should be a positive integer.")
     */
    private ?int $id = null;

    private string $name;

    private bool $isActive = false;

    /**
     * @param ?int $id
     * @param string    $name
     * @param bool     $isActive
     */
    public function __construct(?int $id, string $name, float $isActive)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isActive = $isActive;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getIsActive(): bool
    {
        return $this->isActive;
    }
}
