<?php

namespace App\Customer\Repository;

use App\Entity\Customer;
use App\Repository\AbstractBaseRepository;

final class CustomerDoctrineRepository extends AbstractBaseRepository implements CustomerRepositoryInterface
{

    protected function getEntityClass(): string
    {
        return Customer::class;
    }

    public function getAll(): array
    {
        $qb = $this->createQueryBuilder('c');

        return $qb->getQuery()->getResult();
    }


    public function findById(?int $id): ?Customer
    {
        $qb = $this->createQueryBuilder('c');

        return $qb
            ->where($qb->expr()->eq('c.id', ':id'))
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getTotal(): int
    {
        return count($this->getAll());
    }

    public function add($command)
    {
        $em = $this->getManager();
        $createQuery = $em->getConnection();
        $queryBuilder = $createQuery->createQueryBuilder();

        $name = $command->getName();
        $query = $queryBuilder->insert('customer')
            ->setValue('name', " ' . $name . ' ")
            ->setValue('is_active', $command->getIsActive() ? 1 : 0);
        $query->execute();

        $createQuery->lastInsertId();
        return $this->findById($createQuery->lastInsertId());
    }
}
