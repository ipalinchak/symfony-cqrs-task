<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * AbstractBaseRepository.
 */
abstract class AbstractBaseRepository
{
    public const DEFAULT_ALIAS = 'e';

    /** @var ManagerRegistry */
    private $managerRegistry;

    /**
     * @param ManagerRegistry $managerRegistry
     *
     * @required
     */
    public function setManagerRegistry(ManagerRegistry $managerRegistry): void
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @return QueryBuilder
     */
    public function getBaseQueryBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder(self::DEFAULT_ALIAS);
    }

    /**
     * @return EntityManager
     *
     * @throws \LogicException
     */
    protected function getManager(): EntityManager
    {
        $manager = $this->managerRegistry->getManagerForClass($this->getEntityClass());

        if ($manager instanceof EntityManager) {
            return $manager;
        }

        throw new \LogicException('Could not find entity manager');
    }

    /**
     * @param string $alias
     * @param string $indexBy The index for the from
     *
     * @return QueryBuilder
     */
    protected function createQueryBuilder($alias, $indexBy = null): QueryBuilder
    {
        return $this
            ->getManager()
            ->createQueryBuilder()
            ->select($alias)
            ->from($this->getEntityClass(), $alias, $indexBy)
        ;
    }

    /**
     * @return string
     */
    abstract protected function getEntityClass(): string;
}
