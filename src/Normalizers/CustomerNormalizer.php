<?php

declare(strict_types=1);

namespace App\Normalizers;

use App\Customer\Message\Response\CustomerResponse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * GooglePlaceLocationNormalizer.
 */
class CustomerNormalizer implements NormalizerInterface
{
    /** @var ObjectNormalizer */
    private $objectNormalizer;

    /**
     * @param ObjectNormalizer $objectNormalizer
     */
    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    /**
     * @param CustomerResponse $data
     * @param string|null      $format
     * @param array            $context
     *
     * @return array
     */
    public function normalize($data, $format = null, array $context = []): array
    {
        $data = (array) $this->objectNormalizer->normalize($data, $format, $context);
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($object, $format = null): bool
    {
        return false;
    }
}
