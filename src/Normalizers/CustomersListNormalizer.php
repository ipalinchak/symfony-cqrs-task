<?php

declare(strict_types=1);

namespace App\Normalizers;

use App\Customer\Message\Response\CustomerListResponse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * GooglePlaceLocationNormalizer.
 */
class CustomersListNormalizer implements NormalizerInterface
{
    /** @var ObjectNormalizer */
    private $objectNormalizer;

    /**
     * @param ObjectNormalizer $objectNormalizer
     */
    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    /**
     * @param CustomerListResponse $data
     * @param string|null      $format
     * @param array            $context
     *
     * @return array
     */
    public function normalize($data, $format = null, array $context = []): array
    {
        return [
            'customers'=>$data->getCustomers(),
            'total'=>$data->getTotal()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($object, $format = null): bool
    {
        return false;
    }
}
