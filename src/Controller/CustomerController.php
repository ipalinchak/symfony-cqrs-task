<?php

namespace App\Controller;

use App\Customer\Message\Command\AddCustomerCommand;
use App\Customer\Message\Query\GetCustomerQuery;
use App\Customer\Message\Query\GetCustomersQuery;
use App\Entity\Customer;
use App\Normalizers\CustomerNormalizer;
use App\Normalizers\CustomersListNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/customers")
 */
final class CustomerController extends AbstractController
{
    use ControllerResponseTrait;

    private MessageBusInterface $queryBus;
    private MessageBusInterface $commandBus;
    private NormalizerInterface $normalizer;

    public function __construct(
        MessageBusInterface $queryBus,
        MessageBusInterface $commandBus,
        SerializerInterface $serializer
    ) {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->serializer = $serializer;
    }

    /**
     * @Route("", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function cgetAction(GetCustomersQuery $query, CustomersListNormalizer $normalizer): Response
    {
        $this->normalizer = $normalizer;
        $envelope = $this->queryBus->dispatch($query);

        return $this->createJsonResponseFromEnvelope($envelope);
    }

    /**
     * @Route("/{id}", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function getAction(int $id, CustomerNormalizer $normalizer): Response
    {
        $this->normalizer = $normalizer;
        $query = new GetCustomerQuery($id);
        $envelope = $this->queryBus->dispatch($query);

        return $this->createJsonResponseFromEnvelope($envelope);
    }

    /**
     * @Route("/add", methods={"POST"})
     * @param Request $request
     */
    public function add(Request $request, CustomerNormalizer $normalizer): Response
    {
        $this->normalizer = $normalizer;
        $object = $this->serializer->deserialize($request->getContent(), Customer::class, 'json');
        $command = new AddCustomerCommand(null, $object->getName(), $object->getIsActive());

        $envelope = $this->commandBus->dispatch($command);

        return $this->createJsonResponseFromEnvelope($envelope);
    }
}
